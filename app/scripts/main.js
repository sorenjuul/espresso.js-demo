var Controller  = Espresso.Controller;
var Model = Espresso.Model;

var Settings = Model.extend({
    defaults: {
      initi:false,
      forwardCalls:false,
      whichCallsToForward:'',
      forwardCallsTo:'',
      phoneNumber:''
    }
});

var CallForwarding = Controller.extend({

  init: function() {
    this.ref.alert.hidden = true;
    this.model = new Settings();
    this.listenTo(this.ref.phoneNumber, 'keyup', this.phoneNumberValidate);
  },

  fetchData: function(){

    var self = this;
    if(this.model.get('initi')){return;}
    setTimeout(function(){
      self.set({
        initi:true,
        forwardCalls:false,
        whichCallsToForward:'all',
        forwardCallsTo:'phoneNumber',
        phoneNumber:'55667787'
      });
    },500);
  },

  save: function(){
    var self = this;
    setTimeout(function() {
      self.set({
        initi:true,
        forwardCalls:self.ref.forwardCalls.checked,
        whichCallsToForward:self.ref.whichCallsToForward.value,
        forwardCallsTo:self.ref.forwardCallsTo.value,
        phoneNumber:self.ref.phoneNumber.value
      });
      self.ref.alert.hidden = false;
        setTimeout(function() {self.ref.alert.hidden = true;}, 2000);
    }, 500);
  },

  setForward: function(){
    this.set({forwardCalls: !this.model.get('forwardCalls')});
  },

  changeWhichCallsToForward: function(){
    this.set({whichCallsToForward: this.ref.whichCallsToForward.value})
  },

  changeForwardCallsTo: function(){
    this.set({forwardCallsTo: this.ref.forwardCallsTo.value})
  },

  phoneNumberValidate: function(){
    this.ref.saveWrapper.hidden = this.ref.phoneNumber.value.length !== 8;
    if(this.ref.phoneNumber.value.length !== 8){
      this.ref.phoneNumberWrapper.classList.add("has-error");
    } else {
      this.ref.phoneNumberWrapper.classList.remove("has-error");
    }
  },

  render: function(){
    console.log(this.model);
    return {
      loading: {display: !this.model.get('initi')},
      callForwardWrapper: {display: this.model.get('initi')},
      panel: {onclick: this.fetchData},
      forwardCalls: {checked: this.model.get('forwardCalls'), onclick: this.setForward},
      formWrapper: {display: this.model.get('forwardCalls')},
      whichCallsToForward: {value: this.model.get('whichCallsToForward'), onchange: this.changeWhichCallsToForward},
      forwardCallsTo: {value: this.model.get('forwardCallsTo')},
      forwardCallsToWrapper: {display: this.model.get('whichCallsToForward') !== 'none', onchange: this.changeForwardCallsTo},
      phoneNumber: {value: this.model.get('phoneNumber')},
      phoneNumberWrapper: {display: this.model.get('whichCallsToForward') !== 'none' && this.model.get('forwardCallsTo') === 'phoneNumber'},
      save: {onclick: this.save},
      saveWrapper: {}
    }
  }

});

window.app = new CallForwarding({view: document.getElementById('here')});
